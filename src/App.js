import React from "react";
import CustomizedTables from "./new";
import logo from "./logo.svg";
import "./App.css";

function App() {
    return (
        <div className="App">
            <CustomizedTables />
        </div>
    );
}

export default App;
