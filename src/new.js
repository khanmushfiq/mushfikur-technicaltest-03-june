import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import SearchIcon from "@material-ui/icons/Search";
import InputBase from "@material-ui/core/InputBase";
import { lighten } from "@material-ui/core/styles/colorManipulator";
import { fade } from "@material-ui/core/styles/colorManipulator";

let counter = 0;
let newData = [];
let originalData = [];
function createData(name, calories, fat, carbs, protein) {
  counter += 1;
  return { id: counter, name, calories, fat, carbs, protein };
}

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}


function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === "desc"
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const rows = [
  {
    id: "Segment",
    numeric: false,
    label: "Segment"
  },
  {
    id: "Country",
    numeric: true,
    label: "country"
  },

  { id: "product", numeric: true, disablePadding: false, label: "Phone" },
  { id: "Unit_sold", numeric: true, disablePadding: false, label: "Unit_sold" },
  { id: "mfg_price", numeric: true, disablePadding: false, label: "mfg_price" },
  { id: "sales_price", numeric: true, disablePadding: false, label: "sales_price" },
  { id: "gross_sales", numeric: true, disablePadding: false, label: "gross_sales" }
];

class EnhancedTableHead extends React.Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount
    } = this.props;

    return (
      <TableHead>
        <TableRow>
          {rows.map(
            row => (
              <TableCell
                key={row.id}
                align={row.numeric ? "right" : "left"}
                padding={row.disablePadding ? "none" : "default"}
                sortDirection={orderBy === row.id ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={row.numeric ? "bottom-end" : "bottom-start"}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === row.id}
                    direction={order}
                    onClick={this.createSortHandler(row.id)}
                  >
                    {row.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            ),
            this
          )}
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85)
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
  spacer: {
    flex: "1 1 100%"
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: "0 0 auto"
  },
  grow: {
    flexGrow: 1
  },

  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.black, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.black, 0.25)
    },
    marginLeft: 0,
    // width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing.unit,
      width: "auto"
    },
    color: "#111"
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit",
    width: "100%"
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: 107,
      // height: 23,
      "&:focus": {
        width: 200
      }
    }
  },
  margin: {
    margin: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 3
  }
});

let EnhancedTableToolbar = props => {
  const { numSelected, classes } = props;

  return (
    <Toolbar
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subtitle1">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant="h6" id="tableTitle">
            User List
          </Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div style={{ marginRight: "5px" }}>
        <div className={classes.grow} />
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Search"
            onChange={props.search}
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput
            }}
          />
        </div>
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({});

class EnhancedTable extends React.Component {
  state = {
    fullWidth: true,
    maxWidth: "sm",
    loading: false,
    order: "asc",
    orderBy: "email",
    selected: [],
    data: [{
        segment: "Government",
        country: "Germany",
        product: "xyz",
        unit_sold: 57576,
        mfg_price: 766,
        sales_price: 787,
        gross_sales: 78878
    },
    {
        segment: "Government",
        country: "Germany",
        product: "xyz",
        unit_sold: 57576,
        mfg_price: 766,
        sales_price: 787,
        gross_sales: 78878
    },
    {
        segment: "Midmarket",
        country: "Canada",
        product: "fghj",
        unit_sold: 57576,
        mfg_price: 766,
        sales_price: 787,
        gross_sales: 78878
    },
    {
        segment: "Channel Partners",
        country: "France",
        product: "sfdf",
        unit_sold: 57576,
        mfg_price: 766,
        sales_price: 787,
        gross_sales: 78878
    },
    {
        segment: "Enterprise",
        country: "Mexico",
        product: "xyz",
        unit_sold: 57576,
        mfg_price: 766,
        sales_price: 787,
        gross_sales: 78878
    },
    {
        segment: "Small Business",
        country: "ddggd",
        product: "xyz",
        unit_sold: 57576,
        mfg_price: 766,
        sales_price: 787,
        gross_sales: 78878
    }],
    newData:"",
    originalData:'',
    searched_data:"",
    page: 0,
    rowsPerPage: 5
  };
componentDidMount(){
    this.setState({
        newData: this.state.data,
        originalData: this.state.data
    })
}
  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";

    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.data.map(n => n.id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;
  handleLoader = (load, value) => {
    this.setState({ loading: true });
    if (value) {
      newData =
        // this.state.data.length > 0 &&
        originalData.filter(r => {
          return (
            r.name.toLowerCase().includes(value.toLowerCase()) ||
            r.status.toLowerCase().includes(value.toLowerCase()) ||
            r.email.toLowerCase().includes(value.toLowerCase())
          );
        });
    } else {
      newData = originalData;
    }

    this.setState({ loading: false, data: newData });
  };

  onSearchClicked = e => {
    console.log(e.target.value);
    let value = e.target.value;
    value = value.trim();
    if (value) {
      newData = this.state.originalData.filter(r => {
        return (
          r.segment.toLowerCase().includes(value.toLowerCase()) ||
          r.country.toLowerCase().includes(value.toLowerCase()) 
        );
      });
    } else {
      newData = originalData;
    }

    this.setState({ searched_data: newData });
  };

  render() {
      console.log(this.state.searched_data)
    const { classes } = this.props;
    const { data, order, orderBy, selected, rowsPerPage, page, searched_data } = this.state;
    const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

    return (
      <Paper className={classes.root}>
        <EnhancedTableToolbar
          search={this.onSearchClicked}
          numSelected={selected.length}
        />

        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
            />
            <TableBody>
              {stableSort(data, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(n => {
                  const isSelected = this.isSelected(n.id);
                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, n.id)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.id}
                      selected={isSelected}
                    >
                      <TableCell
                        style={{ paddingLeft: "2em" }}
                        component="th"
                        scope="row"
                        padding="none"
                      >
                        {n.segment}
                      </TableCell>
                      <TableCell align="right">{n.country}</TableCell>
                      <TableCell align="right">{n.product}</TableCell>
                      <TableCell align="right">{n.unit_sold}</TableCell>
                      <TableCell align="right">{n.mfg_price}</TableCell>
                      <TableCell align="right">{n.sales_price}</TableCell>
                      <TableCell align="right">{n.gross_sales}</TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
            
          </Table>
          
        </div>
        <div style={{marginTop:"2em"}}>
              <Table>
              {
                searched_data && searched_data.length > 0 &&
                <TableBody >
              {stableSort(searched_data, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(n => {
                  const isSelected = this.isSelected(n.id);
                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, n.id)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.id}
                      selected={isSelected}
                    >
                      <TableCell
                        style={{ paddingLeft: "2em" }}
                        component="th"
                        scope="row"
                        padding="none"
                      >
                        {n.segment}
                      </TableCell>
                      <TableCell align="right">{n.country}</TableCell>
                      <TableCell align="right">{n.product}</TableCell>
                      <TableCell align="right">{n.unit_sold}</TableCell>
                      <TableCell align="right">{n.mfg_price}</TableCell>
                      <TableCell align="right">{n.sales_price}</TableCell>
                      <TableCell align="right">{n.gross_sales}</TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
            }
              </Table>
          </div>
      </Paper>
    );
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(EnhancedTable);
